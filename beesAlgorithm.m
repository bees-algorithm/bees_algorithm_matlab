function beesAlgorithm()

% This function implements the Bees Algorithm. The user can change the
% type of fitness function (i.e. the optimisation problem), the number of
% optimisation cycles, the number of scouts, the number of elite sites
% (ne), best sites (nb), the number of foragers per elite site (nre), the
% number of foragers per best site (nrb), and the stagnation limit (stlim).
% The algorithm is described in:
% Pham, D.T., Castellani, M. (2009), The Bees Algorithm � Modelling
% Foraging Behaviour to Solve Continuous Optimisation Problems. Proceedings
% ImechE, Part C, vol. 223, no. 12, pp. 2919-2938.
% Remember that: colonySize=(ne*nre+(nb-ne)*nrb)+randomScouts.
% Written by Marco Castellani, Theoretical Ecology Group, University of
% Bergen, email: marco.castellani@bio.uib.no

stepSizePlot=0.5;   % defines the accuracy of the fitness function plot in size units (e.g. if function is defined in interval x=[-10,10], y=[-10,10] and stepSizePlot=1, the plot will be a 20x20 matrix)

% main parameters
if(exist('beesAlgorithmParameters.mat', 'file')==2)
    load BeesAlgorithmParameters;
else
    typeOfFunction='1peak';
    duration=100;
    randomScouts=5;
    ne=2;
    nb=6;
    nre=30;
    nrb=10;
    stlim=10;
end

% fitness function
fitnessEvaluator=FitnessFunction(typeOfFunction);

% user interface
display('The most recently used Bees Algorithm parameters are:');
display(['optimisation function f = ', typeOfFunction]);
display(['optimisation cycles c = ', int2str(duration)]);
display(['random scouts s = ', int2str(randomScouts)]);
display(['elite sites ne = ', int2str(ne)]);
display(['best sites nb = ', int2str(nb)]);
display(['foragers on elite sites nre = ', int2str(nre)]);
display(['foragers on best sites nrb = ', int2str(nrb)]);
display(['stagnation limit stlim = ', int2str(stlim)]);
display('colonySize=(ne*nre+(nb-ne)*nrb)+s');
reply=input('Would you like to change them? y/n [n] ', 's');
if(isempty(reply))
    reply='n';
elseif(~strcmp(reply, 'y')&&~strcmp(reply, 'n'))
    display('not a valid choice, nothing will be changed');
    reply='n';
end
while(~strcmp(reply, 'n'))
    reply=input('What would you like to change? f, c, s, ne, nb, nre, nrb, stlim, n if none [n] ', 's');
    if(strcmp(reply, 'f'))
        display('available functions: ');
        fitnessEvaluator.definedFunctions
        reply=input('enter new function: ', 's');
        if(max(strcmp(fitnessEvaluator.definedFunctions, reply))==1)
            typeOfFunction=reply;
            fitnessEvaluator=FitnessFunction(typeOfFunction);
            display(['optimisation function changed to f = ', fitnessEvaluator.type]);
        else
            display('function entered does not exist, old function kept');
        end
    elseif(strcmp(reply, 'c'))
        reply=input('enter duration of algorithm: ', 's');
        duration=str2double(reply);
        display(['the parameter was changed: ' 'c = ' int2str(duration)]);
    elseif(strcmp(reply, 's'))
        reply=input('enter number of random scouts: ', 's');
        randomScouts=str2double(reply);
        display(['the parameter was changed: ' 's = ' int2str(randomScouts)]);
    elseif(strcmp(reply, 'ne'))
        reply=input('enter number of elite sites: ', 's');
        ne=str2double(reply);
        display(['the parameter was changed: ' 'ne = ' int2str(ne)]);
    elseif(strcmp(reply, 'nb'))
        reply=input('enter number of best sites: ', 's');
        nb=str2double(reply);
        display(['the parameter was changed: ' 'nb = ' int2str(nb)]);
    elseif(strcmp(reply, 'nre'))
        reply=input('enter number of foragers on elite sites: ', 's');
        nre=str2double(reply);
        display(['the parameter was changed: ' 'nre = ' int2str(nre)]);
    elseif(strcmp(reply, 'nrb'))
        reply=input('enter number of foragers on best sites: ', 's');
        nrb=str2double(reply);
        display(['the parameter was changed: ' 'nrb = ' int2str(nrb)]);
    elseif(strcmp(reply, 'stlim'))
        reply=input('enter maximum number of stagnation cycles: ', 's');
        stlim=str2double(reply);
        display(['the parameter was changed: ' 'stlim = ' int2str(stlim)]);
    elseif(strcmp(reply, 'n'))
        display('n chosen, nothing will be changed');
    else
        reply='n';
        display('not a valid choice, nothing will be changed');
    end
end

beeColonySize=(ne*nre+(nb-ne)*nrb)+randomScouts; % size of colony
foragers=(ne*nre+(nb-ne)*nrb);                   % total number of foragers
display(['size of bee colony: ' int2str(beeColonySize)]);
display('please wait...');

%initialisation
totalscouts=nb+randomScouts;
xScout=zeros(1,totalscouts);
yScout=zeros(1,totalscouts);
zScout=zeros(1,totalscouts);
bestFound=zeros(1,duration+1);
step=zeros(1,duration+1);
xForager=zeros(1,totalscouts);
yForager=zeros(1,totalscouts);
zForager=zeros(1,totalscouts);
initialScouts=initialiseBees(totalscouts, fitnessEvaluator);
globalBest=[0 0 0 0];

for i=1:totalscouts
    xScout(i)=initialScouts(i).position(1);
    yScout(i)=initialScouts(i).position(2);
    zScout(i)=initialScouts(i).fitness;
    if(initialScouts(i).fitness>globalBest(3))
        globalBest(1)=initialScouts(i).position(1);
        globalBest(2)=initialScouts(i).position(2);
        globalBest(3)=initialScouts(i).fitness;
        globalBest(4)=0;
    end
    
end

%fitness landscape
xPoints=(fitnessEvaluator.upperBoundaries(1)-fitnessEvaluator.lowerBoundaries(1))/stepSizePlot+1;
yPoints=(fitnessEvaluator.upperBoundaries(2)-fitnessEvaluator.lowerBoundaries(2))/stepSizePlot+1;
fitnessLandscape=zeros(xPoints,yPoints);
abscissa=fitnessEvaluator.lowerBoundaries(1):stepSizePlot:fitnessEvaluator.upperBoundaries(1);
ordinate=fitnessEvaluator.lowerBoundaries(2):stepSizePlot:fitnessEvaluator.upperBoundaries(2);
for i=1:xPoints
    x=fitnessEvaluator.lowerBoundaries(1)+(i-1)*stepSizePlot;
    for j=1:yPoints
        y=fitnessEvaluator.lowerBoundaries(2)+(j-1)*stepSizePlot;
        fitnessLandscape(i,j)=fitnessEvaluator.evaluation(x,y);
    end
end

%plotting initial population
figure(1);
subplot(4,1,1:3)
scatter3(xScout,yScout,zScout,60,'d','r','filled');
hold on;
mesh(abscissa,ordinate,fitnessLandscape);
hold off;
map=((0.2+gray)/1.2)/1.2;
colormap(map);
xlim([fitnessEvaluator.lowerBoundaries(1),fitnessEvaluator.upperBoundaries(1)]);
ylim([fitnessEvaluator.lowerBoundaries(2),fitnessEvaluator.upperBoundaries(2)]);
view(45,75);
title('initial population');
time=0:(duration);
maxFitness(1:(duration+1))=fitnessEvaluator.max;
subplot(4,1,4)
bestFound(1)=globalBest(4);
step(1)=0;
handle(1)=plot(time,maxFitness,'k-');
xlim([0 duration]);
ylim([0 1.1]);
hold on;
handle(2)=plot(step,bestFound,'r-');
hold off;
refresh(1);
display('red dots = scouts, blue dots = foragers');
display('press any key to start');
pause

patches=FlowerPatch.empty(totalscouts,0);
beeColonyForagers=Bee.empty(foragers,0);
sizeNeighbourhood(1)=(fitnessEvaluator.upperBoundaries(1)-fitnessEvaluator.lowerBoundaries(1))/2;   % initial neighbourhood is large, it can be changed to a smaller size.
sizeNeighbourhood(2)=(fitnessEvaluator.upperBoundaries(2)-fitnessEvaluator.lowerBoundaries(2))/2;   % initial neighbourhood is large, it can be changed to a smaller size.
for i=1:totalscouts
    patches(i)=FlowerPatch(initialScouts(i), 0, sizeNeighbourhood);
end

% main loop
for gen=1:duration
    
    % --------------- here starts the Bees Algorithm ----------------------
    % waggle dance, patches get also sorted in descending order of fitness
    patches=waggleDance(patches, totalscouts, ne, nb, nre, nrb);
    % local search, elite
    for b=1:ne
        [patches(b) beeColonyForagers(nre*(b-1)+(1:nre))]=neighbourhoodSearch(patches(b), fitnessEvaluator);
        if(patches(b).stagnated>stlim)
            patches(b)=siteAbandonment(fitnessEvaluator, sizeNeighbourhood);    %site abandonment procedure
        end
    end
    % local search, best
    for b=ne+1:nb
        [patches(b) beeColonyForagers(ne*nre+nrb*(b-ne-1)+(1:nrb))]=neighbourhoodSearch(patches(b), fitnessEvaluator);
        if(patches(b).stagnated>stlim)
            patches(b)=siteAbandonment(fitnessEvaluator, sizeNeighbourhood);    %site abandonment procedure
        end
    end
    % global search
    newScouts=initialiseBees(randomScouts, fitnessEvaluator);
    for b=1:randomScouts
        patches(nb+b)=FlowerPatch(newScouts(b), 0, sizeNeighbourhood);
    end
    % --------------- here ends the Bees Algorithm ------------------------
    
    
    %plotting
    for p=1:totalscouts
        xScout(p)=patches(p).scout.position(1);
        yScout(p)=patches(p).scout.position(2);
        zScout(p)=patches(p).scout.fitness;
        if(patches(p).scout.fitness>globalBest(3))
            globalBest(1)=patches(p).scout.position(1);
            globalBest(2)=patches(p).scout.position(2);
            globalBest(3)=patches(p).scout.fitness;
            globalBest(4)=gen;
        end
    end
    for p=1:foragers
        xForager(p)=beeColonyForagers(p).position(1);
        yForager(p)=beeColonyForagers(p).position(2);
        zForager(p)=beeColonyForagers(p).fitness;
    end
    
    figure(1);
    subplot(4,1,1:3)
    scatter3(xScout,yScout,zScout+0.05,120, 'r','filled');
    hold on;
    scatter3(xForager,yForager,zForager+0.05,30, 'b','filled');
    hold on;
    mesh(abscissa,ordinate,fitnessLandscape);
    hold off;
    map=(0.5+gray)/1.6;
    colormap(map);
    xlim([fitnessEvaluator.lowerBoundaries(1),fitnessEvaluator.upperBoundaries(1)]);
    ylim([fitnessEvaluator.lowerBoundaries(2),fitnessEvaluator.upperBoundaries(2)]);
    view(45,75);
    title(strcat(['step: ', int2str(gen)]));
    subplot(4,1,4)
    bestFound(gen+1)=globalBest(3);
    step(gen+1)=gen;
    handle(1)=plot(time,maxFitness,'k-');
    xlim([0 duration]);
    ylim([0 1]);
    title({' '; strcat(['best so far: ', num2str(globalBest(3)), ' (gen ', int2str(globalBest(4)), ')', ' - optimum: ', num2str(fitnessEvaluator.max)])});
    hold on;
    handle(2)=plot(step(1:gen+1),bestFound(1:gen+1),'r-');
    hold off;
    refresh(1);
    
end

save('beesAlgorithmParameters','typeOfFunction', 'duration', 'randomScouts', 'ne', 'nb', 'nre', 'nrb', 'stlim');
display('end of simulation');
