function population = updateNeighbourhoodBest(population, populationSize)

for p=1:populationSize
    for n=1:population(p).neighbours;
        neighbour=p+n;
        if(neighbour>populationSize)
            neighbour=rem(p+n, populationSize);
        end
        if(population(neighbour).personalBest(3)>population(p).personalBest(3))
            population(p).neighbourhoodBest=population(neighbour).personalBest;
        end
    end
end



