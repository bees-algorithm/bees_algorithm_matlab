function forager=foragerPlacer(lowerLimits, upperLimits, fitnessEvaluator)

newPosition(1)=random('unif', lowerLimits(1), upperLimits(1));
newPosition(2)=random('unif', lowerLimits(2), upperLimits(2));
forager=Bee(newPosition);   % create forager
forager.fitness=fitnessEvaluator.evaluation(forager.position(1), forager.position(2)); % evaluate forager

