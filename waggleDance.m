function rankedPool = waggleDance(patches, numberOfPatches, eliteBees, bestBees, eliteRecruits, bestRecruits)

rankedPool=FlowerPatch.empty(numberOfPatches, 0);
fitnessList=zeros(numberOfPatches,2);
for p=1:numberOfPatches
    fitnessList(p, 1)=p;
    fitnessList(p, 2)=patches(p).scout.fitness;
end
% sort list of solutions according to fitness
rankedList=sortrows(fitnessList, -2); 
% instantiate ranked vector of flower patches and assign foragers
for p=1:eliteBees
    rankedPool(p)=patches(rankedList(p, 1));
    rankedPool(p).recruits=eliteRecruits;
end
for p=eliteBees+1:bestBees
    rankedPool(p)=patches(rankedList(p, 1));
    rankedPool(p).recruits=bestRecruits;
end
for p=bestBees+1:numberOfPatches
    rankedPool(p)=patches(rankedList(p, 1));
    rankedPool(p).recruits=0;
end