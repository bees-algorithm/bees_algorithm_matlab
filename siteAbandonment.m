function flowerPatch = siteAbandonment(fitnessEvaluator, sizeNeighbourhood)

    position=random('unif', fitnessEvaluator.lowerBoundaries, fitnessEvaluator.upperBoundaries);
    scout=Bee(position);                                        % generate new random scout
    flowerPatch=FlowerPatch(scout, 0, sizeNeighbourhood);       % reinitialise flower patch
end
