classdef FlowerPatch
    %UNTITLED7 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        scout;                      % scoutBee
        recruits;                  % number of recruited bees
        sizePatch;                % half side of neighbourhood centred on scout bee
        stagnated;               % number of cycles of stagnation
    end
    
    methods
        function obj = FlowerPatch(scoutBee, followers, neighbourhood)
            obj.scout=scoutBee;
            obj.recruits=followers;
            obj.sizePatch=neighbourhood;
            obj.stagnated=0;
        end
    end
    
end

