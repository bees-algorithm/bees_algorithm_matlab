function population = initialiseBees(populationSize, fitnessEvaluator)

population=Bee.empty(populationSize, 0);

for i=1:populationSize
    position=random('unif', fitnessEvaluator.lowerBoundaries, fitnessEvaluator.upperBoundaries);
    population(i)=Bee(position);
    population(i).fitness=fitnessEvaluator.evaluation(position(1), position(2));
end