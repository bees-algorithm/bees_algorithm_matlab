# Bees Algorithm

This is the main repository of the Bees Algorithm and contains an implementation in Matlab.

The aim is to make available to everyone a library, built with the minimal number of dependencies, which can be easily integrated in larger projects, as well as used out-of-the-box to solve specific problems.
The code here presented is a modified version of the standard Matlab Bees Algorithm implementation that can be found on the [official Bees Algorithm website](http://beesalgorithmsite.altervista.org).

The Bees Algorithm is an intelligent optimization technique belonging to the swarm algorithms field.
Given a parametric objective function, the goal of the algorithm is to find the parameter values that maximise/minimise the output of the objective function.

Many real-world problems can be modeled as the optimisation of a parametric objective function, therefore effective algorithms to handle this kind of problems are of primary importance in many fields.
The Bees Algorithm performs simultaneus aggressive local searches around the most promising parameter settings of the objective function.
The algorithm is proven to outperform other intelligent optimisation techniques in many benchmark functions<sup>[2][3][4]</sup> as well as real world problems.

On top of this Matlab version, implmentations of the bees Algorithm in [Python](https://gitlab.com/bees-algorithm/bees_algorithm_python) and [C++](https://gitlab.com/bees-algorithm/bees_algorithm_cpp) are also available in the respective repositories. 

# Introduction on the Bees Algorithm
<img src="https://images.pexels.com/photos/1043059/pexels-photo-1043059.jpeg"
     align="left"
     width="333" height="250" />

The Bees Algorithm is a nature-inspired search method that mimics the foraging behaviour of honey bees. It was created by [Prof. D.T. Pham](https://www.birmingham.ac.uk/staff/profiles/mechanical/pham-duc.aspx) and his co-workers in 2005<sup>[1]</sup>, and described in its standard formulation by Pham and Castellani<sup>[2]</sup>.

The algorithm uses a population of agents (artificial bees) to sample the solution space. A fraction of the population (scout bees) searches randomly for regions of high fitness (global search). The most successful scouts recruit a variable number of idle agents (forager bees) to search in the proximity of the fittest solutions (local search). Cycles of global and local search are repeated until an acceptable solution is discovered, or a given number of iterations have elapsed.

The standard version of the Bees Algorithm includes two heuristics: *neighbourhood shrinking* and *site abandonment*.
Using neighbourhood shrinking the size of the local search is progressively reduced when local search stops progressing on a given site.
The site abandonment procedure interrupts the search at one site after a given number of consecutive stagnation cycles, and restarts the local search at a randomly picked site.


<img src="pics/algorithm.png"
     align="right"
     width="300" height="300" />

The algorithm requires a number of parameters to be set, namely: the number of scout bees (*ns*), number of sites selected out of *ns* visited sites (*nb*), number of elite sites out of *nb* selected sites (*ne*), number of bees recruited for the best *ne* sites (*nre*), number of bees recruited for the other (*nb-ne*) selected sites (*nrb*).
The heuristics also require the set of the initial size of the patches (*ngh*) and the number of cycles after which a site is abandoned (*stlim*).
Finally, the stopping criterion must be defined. 

The algorithm starts with the *ns* scout bees being placed randomly in the search space and the main algorithm steps can be summarised as follows:


1. Evaluate the fitness of the population according the objective function;
2. Select the best *nb* sites for neighbourhood (local) search;
3. Recruit *nrb* forager bees for the selected sites (*nre* bees for the best *ne* sites) and evaluate their fitnesses;
4. Select the fittest bee from each local site as the new site centre;
5. If a site fails to improve in a single local search, its neighbourhood size is reduced (neighbourhood shrinking);
6. If a site fails to improve for *stlim* cycles, the site is abandoned (site abandonment);
7. Assign the remaining bees to search uniformly the whole search space and evaluate their fitnesses;
8. If the stopping criterion is not met, return to step 2;

For more information please refer to the [official Bees Algorithm website](http://beesalgorithmsite.altervista.org) and the [wikipedia page](https://en.wikipedia.org/wiki/Bees_algorithm).

# Usage
This implementation is for didactic purposes, and allows the user to choose between 6 different fitness landscapes. By modifying the file FitnessFunction.m the user can add more fitness landscapes.

# References

- [1]: Pham, Duc Truong, et al. "[The Bees Algorithm—A Novel Tool for Complex Optimisation Problems.](https://s3.amazonaws.com/academia.edu.documents/37269572/Pham06_-_The_Bee_Algorithm.pdf?AWSAccessKeyId=AKIAIWOWYYGZ2Y53UL3A&Expires=1550167392&Signature=%2BAoKD8QFeUIXvpEjojuAxWu2y0k%3D&response-content-disposition=inline%3B%20filename%3DThe_Bees_Algorithm_-_A_Novel_Tool_for_Co.pdf)" Intelligent production machines and systems. 2006. 454-459.
- [2]: Pham, Duc Truong, and Michele Castellani. "[The bees algorithm: modelling foraging behaviour to solve continuous optimization problems.](https://www.researchgate.net/profile/Marco_Castellani2/publication/229698041_The_Bees_Algorithm_Modelling_foraging_behaviour_to_solve_continuous_optimization_problems/links/0912f50107f349b7de000000/The-Bees-Algorithm-Modelling-foraging-behaviour-to-solve-continuous-optimization-problems.pdf)" Proceedings of the Institution of Mechanical Engineers, Part C: Journal of Mechanical Engineering Science 223.12 (2009): 2919-2938.
- [3]: Pham, Duc Truong, and Marco Castellani. "[Benchmarking and comparison of nature-inspired population-based continuous optimisation algorithms.](https://link.springer.com/article/10.1007/s00500-013-1104-9)" Soft Computing 18.5 (2014): 871-903.
- [4]: Pham, Duc Truong, and Marco Castellani. "[A comparative study of the Bees Algorithm as a tool for function optimisation.](https://www.tandfonline.com/doi/full/10.1080/23311916.2015.1091540)" Cogent Engineering 2.1 (2015): 1091540.

For more references please refer to the README file in the [C++ repository](https://gitlab.com/bees-algorithm/bees_algorithm_cpp).

# Authors and License
The code was designed and developed by [Marco Castellani](https://www.birmingham.ac.uk/staff/profiles/mechanical/castellani-marco.aspx) (m.castellani[at]bham.ac.uk), as it can also be found on the Bees Algorithm [official site](http://beesalgorithmsite.altervista.org/).

This library is released under [GPL v3 license](LICENSE).
