classdef Agent
    %UNTITLED2 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        position;                                   % (x,y) actual location in space
        fitness;                                    %  fitness of object
    end
    
    % construct an Agent object using the coefficients supplied
    methods
        function obj = Agent(coordinates)
            obj.position=coordinates;
            obj.fitness=0;
        end
    end
    
end

