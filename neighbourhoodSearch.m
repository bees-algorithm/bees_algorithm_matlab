function [flowerPatch recruits] = neighbourhoodSearch(flowerPatch, fitnessEvaluator)

neighbourhood=flowerPatch.sizePatch;
bestFitness=flowerPatch.scout.fitness+0.00001;  % 0.00001 is the resolution of the search. Otherwise, the search could get stuck on infinitesimal improvements.  
stagnated=true;
recruits=Bee.empty(flowerPatch.recruits, 0);

% width of flower patch
upperLimits(1)=min(fitnessEvaluator.upperBoundaries(1),flowerPatch.scout.position(1)+neighbourhood(1));
lowerLimits(1)=max(fitnessEvaluator.lowerBoundaries(1),flowerPatch.scout.position(1)-neighbourhood(1));
upperLimits(2)=min(fitnessEvaluator.upperBoundaries(2),flowerPatch.scout.position(2)+neighbourhood(2));
lowerLimits(2)=max(fitnessEvaluator.lowerBoundaries(2),flowerPatch.scout.position(2)-neighbourhood(2));

% place foragers in neighbourhood of site marked by scout.
for r=1:flowerPatch.recruits
    recruits(r)=foragerPlacer(lowerLimits, upperLimits, fitnessEvaluator);
    if(recruits(r).fitness>bestFitness) % if a forager finds a site of higher fitness than that indicated by the scout, that forager becomes the new scout.
        flowerPatch.scout=recruits(r);
        stagnated=false;
        bestFitness=flowerPatch.scout.fitness;
    end
end

if(~stagnated)
    flowerPatch.stagnated=0;              % if the local search has found a better solution, set the stagnation counter to zero.
else                                      % % if the local search has not found a better solution, neighbourhood shrinking.
    flowerPatch.stagnated=flowerPatch.stagnated+1;          % increase the stagnation counter of one cycle.
    flowerPatch.sizePatch=0.8*flowerPatch.sizePatch;        % shrink neighbourhood.
end
