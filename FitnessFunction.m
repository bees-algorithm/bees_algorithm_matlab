classdef FitnessFunction
    
    properties
        type;                                           % type of fitness function (2peaks)
        lowerBoundaries;                                % minimum x andy coordinates
        upperBoundaries;                                % maximum x andy coordinates
        max                                             % global maximum
        definedFunctions                                % functions defined in this class
    end
    
    methods
        function obj = FitnessFunction(typeOfFunction)
            obj.definedFunctions={'1peak' '2peaks' '4peaks' '6peaks' '8peaks' '10peaks'};
            obj.type=typeOfFunction;
            obj.lowerBoundaries=[-100,-100];
            obj.upperBoundaries=[100,100];
            if( strcmp(obj.definedFunctions(1),obj.type))
                obj.max=evaluation(obj, 0, 0);
            elseif(strcmp(obj.definedFunctions(2),obj.type))
                obj.max=evaluation(obj, -40, -40);
            elseif(strcmp(obj.definedFunctions(3),obj.type))
                obj.max=evaluation(obj, -60, -60);
            elseif(strcmp(obj.definedFunctions(4),obj.type))
                obj.max=evaluation(obj, -60, -60);
            elseif(strcmp(obj.definedFunctions(5),obj.type))
                obj.max=evaluation(obj, -60, -60);
            elseif(strcmp(obj.definedFunctions(6),obj.type))
                obj.max=evaluation(obj, -60, -60);
            else
                disp('fitness function not defined');
                return;
            end
            
        end
        
        function fitness = evaluation(obj, x, y)
            
            if(strcmp(obj.definedFunctions(1),obj.type))
                f1=(0-x)/100;
                f2=(0-y)/100;
                p1=power(f1,2);
                p2=power(f2,2);
                c1=exp(-(p1+p2));   % main peak
                fitness=c1;
            elseif(strcmp(obj.definedFunctions(2),obj.type))
                f1=(-40-x)/20;
                f2=(-40-y)/20;
                p1=power(f1,2);
                p2=power(f2,2);
                c1=exp(-(p1+p2));       % main peak
                f1=(40-x)/20;
                f2=(40-y)/20;
                p1=power(f1,2);
                p2=power(f2,2);
                c2=0.8*exp(-(p1+p2));   % secondary peak
                fitness=c1+c2;
            elseif(strcmp(obj.definedFunctions(3),obj.type))
                f1=(-60-x)/30;
                f2=(-60-y)/30;
                p1=power(f1,2);
                p2=power(f2,2);
                c1=exp(-(p1+p2));       % main peak
                f1=(40-x)/30;
                f2=(40-y)/30;
                p1=power(f1,2);
                p2=power(f2,2);
                c2=0.8*exp(-(p1+p2));   % secondary peak
                f1=(-40-x)/30;
                f2=(40-y)/30;
                p1=power(f1,2);
                p2=power(f2,2);
                c3=0.8*exp(-(p1+p2));   % secondary peak
                f1=(40-x)/30;
                f2=(-40-y)/30;
                p1=power(f1,2);
                p2=power(f2,2);
                c4=0.8*exp(-(p1+p2));   % secondary peak
                fitness=c1+c2+c3+c4;
            elseif(strcmp(obj.definedFunctions(4),obj.type))
                f1=(-60-x)/20;
                f2=(-60-y)/20;
                p1=power(f1,2);
                p2=power(f2,2);
                c1=exp(-(p1+p2));       % main peak
                f1=(40-x)/20;
                f2=(40-y)/20;
                p1=power(f1,2);
                p2=power(f2,2);
                c2=0.6*exp(-(p1+p2));   % secondary peak
                f1=(-40-x)/20;
                f2=(40-y)/20;
                p1=power(f1,2);
                p2=power(f2,2);
                c3=0.6*exp(-(p1+p2));   % secondary peak
                f1=(40-x)/20;
                f2=(-40-y)/20;
                p1=power(f1,2);
                p2=power(f2,2);
                c4=0.6*exp(-(p1+p2));   % secondary peak
                f1=(0-x)/20;
                f2=(0-y)/20;
                p1=power(f1,2);
                p2=power(f2,2);
                c5=0.6*exp(-(p1+p2));   % secondary peak
                f1=(80-x)/20;
                f2=(0-y)/20;
                p1=power(f1,2);
                p2=power(f2,2);
                c6=0.6*exp(-(p1+p2));   % secondary peak
                fitness=c1+c2+c3+c4+c5+c6;
            elseif(strcmp(obj.definedFunctions(5),obj.type))
                f1=(-60-x)/20;
                f2=(-60-y)/20;
                p1=power(f1,2);
                p2=power(f2,2);
                c1=exp(-(p1+p2));       % main peak
                f1=(40-x)/20;
                f2=(40-y)/20;
                p1=power(f1,2);
                p2=power(f2,2);
                c2=0.6*exp(-(p1+p2));   % secondary peak
                f1=(-40-x)/20;
                f2=(40-y)/20;
                p1=power(f1,2);
                p2=power(f2,2);
                c3=0.6*exp(-(p1+p2));   % secondary peak
                f1=(40-x)/20;
                f2=(-40-y)/20;
                p1=power(f1,2);
                p2=power(f2,2);
                c4=0.6*exp(-(p1+p2));   % secondary peak
                f1=(0-x)/20;
                f2=(0-y)/20;
                p1=power(f1,2);
                p2=power(f2,2);
                c5=0.6*exp(-(p1+p2));   % secondary peak
                f1=(80-x)/20;
                f2=(0-y)/20;
                p1=power(f1,2);
                p2=power(f2,2);
                c6=0.6*exp(-(p1+p2));   % secondary peak
                f1=(80-x)/20;
                f2=(80-y)/20;
                p1=power(f1,2);
                p2=power(f2,2);
                c7=0.6*exp(-(p1+p2));   % secondary peak
                f1=(0-x)/20;
                f2=(80-y)/20;
                p1=power(f1,2);
                p2=power(f2,2);
                c8=0.6*exp(-(p1+p2));   % secondary peak
                fitness=c1+c2+c3+c4+c5+c6+c7+c8;
            elseif(strcmp(obj.definedFunctions(6),obj.type))
                f1=(-60-x)/20;
                f2=(-60-y)/20;
                p1=power(f1,2);
                p2=power(f2,2);
                c1=exp(-(p1+p2));       % main peak
                f1=(40-x)/20;
                f2=(40-y)/20;
                p1=power(f1,2);
                p2=power(f2,2);
                c2=0.6*exp(-(p1+p2));   % secondary peak
                f1=(-40-x)/20;
                f2=(40-y)/20;
                p1=power(f1,2);
                p2=power(f2,2);
                c3=0.6*exp(-(p1+p2));   % secondary peak
                f1=(40-x)/20;
                f2=(-40-y)/20;
                p1=power(f1,2);
                p2=power(f2,2);
                c4=0.6*exp(-(p1+p2));   % secondary peak
                f1=(0-x)/20;
                f2=(0-y)/20;
                p1=power(f1,2);
                p2=power(f2,2);
                c5=0.6*exp(-(p1+p2));   % secondary peak
                f1=(80-x)/20;
                f2=(0-y)/20;
                p1=power(f1,2);
                p2=power(f2,2);
                c6=0.6*exp(-(p1+p2));   % secondary peak
                f1=(80-x)/20;
                f2=(80-y)/20;
                p1=power(f1,2);
                p2=power(f2,2);
                c7=0.6*exp(-(p1+p2));   % secondary peak
                f1=(0-x)/20;
                f2=(80-y)/20;
                p1=power(f1,2);
                p2=power(f2,2);
                c9=0.6*exp(-(p1+p2));   % secondary peak
                f1=(-80-x)/20;
                f2=(80-y)/20;
                p1=power(f1,2);
                p2=power(f2,2);
                c10=0.6*exp(-(p1+p2));   % secondary peak
                f1=(80-x)/20;
                f2=(-80-y)/20;
                p1=power(f1,2);
                p2=power(f2,2);
                c8=0.6*exp(-(p1+p2));
                fitness=c1+c2+c3+c4+c5+c6+c7+c8+c9+c10;
            end
            
        end
        
        
    end
    
end

